# dullweber2024

This repository contains codes to reproduce the results presented in Dullweber et al. "Shape switching and tunable oscillations of adaptive droplets" as well as the companion article "Feedback between microscopic activity and macroscopic dynamics drives excitability
and oscillations in mechanochemical matter". There are four directories:

- Mathematica
- MatCont
- Surface Evolver
- SBI

Note that each directory has its own README file further explaining the provided content.

The **Mathematica** directory contains two Mathematica notebooks, that allow to reproduce most of the figures presented in both articles. They also contain additional, simulation-based analyses of the attractor states---not shown in the paper---to verify the results of numerical continuation with a step-by-step explanation. The notebooks are structured according to the order in which the figures appear in the papers. Notebooks can be examined using the [free wolfram player](https://www.wolfram.com/player/). Note that the final form of the figures presented in both articles were prepared using [Inkscape](https://inkscape.org).

The **MatCont** directory contains MATLAB scripts that allow to reproduce the continuation-based bifurcation analysis presented in the two papers. For this work, the authors used MatCont7p4 [A. Dhooge et al., 2008]. MatCont can be found here for download: [MatCont](https://sourceforge.net/projects/matcont/). After download, move the folder *MatCont7p4* into the **MatCont** directory.

The **Surface Evolver** directory contains the code to reproduce the numerical surface energy minimization presented in "Shape switching and tunable oscillations of adaptive droplets" Fig.1 - using the finite-element-based software Surface Evolver [K. Brakke, 1992] (Software can be found here for download: [Surface Evolver](http://kenbrakke.com/evolver/html/install.htm)).

The **SBI** directory contains python scripts that allow to recapitulate the simulation-based inference analysis (based on the python package [A. Tejero-Cantero, 2020]: [sbi](https://github.com/sbi-dev/sbi)) to model contact-angle measurements in zebrafish embryos presented in "Shape switching and tunable oscillations of adaptive droplets". 
