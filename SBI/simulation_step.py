import pickle, torch, numpy as np

# Script arguments
# Example: python simulation_step.py -b 1000

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-m', dest = 'model', type = str, default = 'model')
parser.add_argument('-b', dest = 'batch', type = int, default = 100000)
parser.add_argument('-n', dest = 'num_workers', type = int, default = 3)
parser.add_argument('--gen', dest = 'genotype', type = str, default = "WT")
parser.add_argument('--hill', dest = 'hill', type = int, default = 2)
parser.add_argument('--relerr', dest = 'rel_err', type = float, default = 0.1)
args = parser.parse_args()

# Load y-positions for dataset
positions = np.loadtxt(f'./positions_g-{args.genotype}.txt', delimiter=',')

# Load tension ratios from dataset
tension_ratios = np.loadtxt(f'./tension_ratios_g-{args.genotype}.txt', delimiter=',') 

# Prepare model
mdl = __import__(args.model)
Model = getattr(mdl, "Model")
if positions is None:
  model = Model()
else:
  model = Model(positions = positions)
prior = getattr(mdl, "PRIOR")
model.hill = args.hill
model.rel_angle_error = args.rel_err

# Prepare SBI
from sbi.inference import prepare_for_sbi, simulate_for_sbi
simulator, proposal = prepare_for_sbi(model, prior)

# Prepare random number for seeding the simulator
seed = int(torch.randint(1000000, size=(1,))) if args.seed is None else args.seed

# Run simulations
theta, x = simulate_for_sbi(simulator, proposal,
    num_simulations = args.batch, num_workers = args.num_workers, seed=seed
)

out = f'./training_data_g-{args.genotype}_h-{model.hill}_e-{model.rel_angle_error}.pkl'
torch.save(torch.column_stack((theta, x)), out)
print(f"Simulations saved to {out}")
print(f"{args.num_workers} workers were used for simulations")
