import pickle, torch, numpy as np

# Script arguments
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-m', dest='model', type=str, default='model')
parser.add_argument('--gen', dest = 'genotype', type = str, default = "WT")
parser.add_argument('--hill', dest = 'hill', type = int, default = 2)
parser.add_argument('--relerr', dest = 'rel_err', type = float, default = 0.1)
parser.add_argument('--degree', dest = 'degree', type = int, default = 10)
args = parser.parse_args()

# Load y-positions for dataset
positions = np.loadtxt(f'./positions_g-{args.genotype}.txt', delimiter=',')

# Load tension ratios from dataset
tension_ratios = np.loadtxt(f'./tension_ratios_g-{args.genotype}.txt', delimiter=',') 

# Prepare model
mdl = __import__(args.model)
Model = getattr(mdl, "Model")
if positions is None:
    model = Model()
else:
    model = Model(positions = positions)
prior = getattr(mdl, "PRIOR")
model.hill = args.hill
model.rel_angle_error = args.rel_err
model.degree = args.degree

# Prepare data
data = torch.load(f'./training_data_g-{args.genotype}_h-{model.hill}_e-{model.rel_angle_error}.pkl')
theta = data[:, :prior._event_shape[0]]
#x = data[:, prior._event_shape[0]:]
x = torch.cat(
    (data[:, prior._event_shape[0]:prior._event_shape[0] + (args.degree-1)],  # Select next args.degree columns, -1 because 1.order not part of summary statistic
     data[:, -4:]),  # Always include the 4 correlation coefficients
    dim=1  # Concatenate along the column dimension
)
print(f'Training with {len(data)} data points, degree={args.degree}')


# Prepare SBI
from sbi.inference import SNPE
inference = SNPE(prior=prior, density_estimator='nsf') # 'nsf', 'mdn'
density_estimator = inference.append_simulations(theta, x, proposal=prior).train()
proposal = inference.build_posterior(density_estimator)

# Save posterior and inference object
dpr_out = f'./posterior_g-{args.genotype}_h-{model.hill}_e-{model.rel_angle_error}_d-{model.degree:02}.pickle'
dinf_out = f'./inference_g-{args.genotype}_h-{model.hill}_e-{model.rel_angle_error}_d-{model.degree:02}.pickle'
with open(dpr_out, 'wb') as handle: pickle.dump(proposal, handle)
print(f'Posterior saved to {dpr_out}')
with open(dinf_out, 'wb') as handle: pickle.dump(inference, handle)
print(f'Inference object saved to {dinf_out}')
