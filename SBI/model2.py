"""Model of simulation"""
import numpy as np
import numpy.typing as npt
import torch
from sbi import utils as utils
from scipy.integrate import solve_ivp
from numba import jit

PRIOR = utils.BoxUniform(
    low = torch.tensor([
        0.7,  #gamma0_low,
        0,  #gAg0ratio_low,
        0,  #c0_low,
        0   #hill_low,
    ]),
    high = torch.tensor([
        1,  #gamma0_high,
        1,  #gAg0ratio_high,
        20, #c0_high,
        8   #hill_high
    ])
)    

# Define helper functions outside the class
@jit(nopython=True)
def tension_ratio(u, gamma0, gammaA):
    return gamma0 - gammaA * u**2

@jit(nopython=True)
def area(tension):
    return 6 * 2 ** (1 / 3) * (1 - tension)

@jit(nopython=True)
def sigma(s, h):
    return s**h/(1 + s**h)

@jit(nopython=True)
def signal(chi, area):
    return chi * area

@jit(nopython=True)
def tension_error(tension: float, angle_error: float) -> float:
    return 0.5 * np.sqrt(1 - tension**2)*angle_error

class Model:
    """Simulation model

    Attributes:
        PRIOR (torch.distributions.Independent): Prior ranges of the simulation parameters
            (class attribute)
        sample_size (int): How many times to sample the distribution of chi
        basis (list[numpy.polynomial.Legendre]): Number of Legendre modes to use in the summary statistics
    """

    def __init__(self, **kwargs):
        """Initialize model "metaparameters"

        Args: 
            sample_size (int): Size of the sample to simulate the distribution of contact angles
            basis_power (int): Number of Legendre modes to use in the summary statistics
        """
        # signal gradient decay constant
        self.kappa = kwargs.pop('kappa', 10)
        # y-positions along av-axis at which simulations are evaluated (should be informed form data)
        self.positions = kwargs.pop('positions', np.linspace(0,1,1000))
        # degree of legendre polynomials used for summary statistics
        self.degree = kwargs.pop('degree', 9)
        # number of simulations that will be run
        self.sample_size = len(self.positions) 
        # RELATIVE error of contact angle measurements [rad]
        self.rel_angle_error = kwargs.pop('angle_error', 0.15)

    def __call__(self, params):
        """Sample distributions of parameters and return a summary statistic for tension ratios"""
        (
            gamma0,
            gAg0ratio,
            c0,
            hill
        ) = params

        tension_ratios = np.zeros(self.sample_size)
        
        # draw a random chi value for each position
        chi_values = [np.random.gamma(c0*np.exp(-self.kappa * self.positions[i]), scale = 1) for i in range(self.sample_size)]
        # compute gammaA
        gammaA = gAg0ratio*gamma0
        
        # ensure gamma0, gammaA and hill of numpy type
        gamma0 = np.asarray(gamma0)
        gammaA = np.asarray(gammaA)
        hill = np.asarray(hill)
        
        # run simulations
        for n in range(self.sample_size):
            
            # running a single simulation
            tension_ratios[n] = self.do_one_simulation(gamma0, gammaA, chi_values[n], hill)
        
        return self.summary_statistics(tension_ratios)
     
        
    def summary_statistics(self, sample: npt.ArrayLike, degree=None) -> npt.NDArray[np.float64]:
        """Calculate unnormalized Legendre modes from a sample

        Args:
            sample: Flat array of values on the domain [0, 1]
        Returns:
            Amplitudes of unnormalized Legendre modes
        """
        
        # correlation to encode information about spatial gradients
        corr11 = np.sum([self.positions[i] * sample[i] for i in range(len(sample)) ])/len(sample)
        corr21 = np.sum([self.positions[i]**2 * sample[i] for i in range(len(sample)) ])/len(sample)
        corr12 = np.sum([self.positions[i] * sample[i]**2 for i in range(len(sample)) ])/len(sample)
        corr22 = np.sum([self.positions[i]**2 * sample[i]**2 for i in range(len(sample)) ])/len(sample)
        
        
        if degree is None:
            degree = self.degree

        return np.concatenate((
                np.asarray([
                  np.polynomial.legendre.Legendre.basis(
                    n, domain = [0, 1]
                  )(sample).mean()
                  for n in range(1,degree)
                ]),
                np.array([corr11, corr21, corr12, corr22])
        ))
        
    def do_one_simulation(self, gamma0, gammaA, chi, hill):
        """Perform a single simulation using advanced ODE solver"""

        def ode(t, u):
            current_area = area(tension_ratio(u, gamma0, gammaA))
            du_dt = sigma(signal(chi, current_area), hill) - u
            return du_dt

        tmax = 50
        # Initial internal states
        u0 = np.random.rand(1)  
        # Solve ode
        sol = solve_ivp(ode, [0, tmax], u0, method='RK45', t_eval=np.linspace(0, tmax, int(tmax / 0.01)))
        # get final state
        u = sol.y[:, -1]
        # return tension ratio
        tension = tension_ratio(u, gamma0, gammaA)  
        # convert tension into contact angle
        angle = 2*np.arccos(tension)

        # consider a relative error of contact angle measurement
        angle = max(0,np.random.normal(loc=angle, scale=angle*self.rel_angle_error))
        
        # consider an absolute error of contact angle measurement
        #angle = max(0,np.random.normal(loc=angle, scale=self.abs_angle_error*np.pi/180))
        
        # compute back into tension ratio
        tension = np.cos(angle/2)
        
        return tension
        

    def tension_ratios(self, params, batch=1000):
        """Sample distribution of parameters and return a distribution of tension ratios"""
        (
            gamma0,
            gAg0ratio,
            c0,
            hill
        ) = params
 
        tension_ratios = np.zeros(self.sample_size)
        
        # draw a random chi value for each position
        chi_values = [np.random.gamma(c0*np.exp(-self.kappa * self.positions[i]), scale = 1) for i in range(len(self.positions))]
        # compute gammaA
        gammaA = gAg0ratio*gamma0
        
        # ensure gamma0, gammaA and chi are numpy arrays
        gamma0 = np.asarray(gamma0)
        gammaA = np.asarray(gammaA)
        
        # run simulations
        for n in range(self.sample_size):
            
            # running a single simulation
            tension_ratios[n] = self.do_one_simulation(gamma0, gammaA, chi_values[n], hill)
        
        return tension_ratios 
    
    def reconstruct_pdf(self, modes: npt.NDArray[np.float64], nodes: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
        """Reconstruct values of PDF on a grid from unnormalized Legendre modes
        
        Args:
            modes: Amplitudes of unnormalized Legendre modes
            nodes: Grid nodes on which PDF is evaluated
        
        Returns:
            Values of PDF
        """
        return np.einsum(
            'm,mn', modes, [
                (2 * n + 1) * np.polynomial.legendre.Legendre.basis(
                    n, domain = [0, 1]
                )(nodes) for n in range(len(modes))
            ]
        )

