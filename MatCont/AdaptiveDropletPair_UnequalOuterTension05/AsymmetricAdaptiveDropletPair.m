function out = AsymmetricAdaptiveDropletPair
out{1} = @init;
out{2} = @fun_eval;
% out{3} = @jacobian;
% out{4} = @jacobianp;
% out{5} = @hessians;
% out{6} = @hessiansp;
out{3} = [];
out{4} = [];

out{5} = [];
out{6} = [];
out{7} = [];
out{8} = [];
out{9} = [];

% --------------------------------------------------------------------------
function dydt = fun_eval(t, kmrgd, par_gammaA, par_chi, par_gamma0)
h=4;
% consider gamma0 as gamma0/2gammaf
% consider gammaA as gammaA/2gammaf
% consider gammac as gammac/2gammaf
gammac = par_gamma0 - par_gammaA*kmrgd(1)*kmrgd(2); 
syms areaFunc(x);
areaFunc(x) = piecewise( ...
    x>0 & x <= 0.5, 2^(4/3), ...
    x>0.5 & x <= 0.65, -2.62194 + 2.60774/(x-0.496457).^0.120104, ...
    x>0.65 & x<1, 7.13279 - 20.2205*x + 16.1909*x^2 + 8.20926*x^3 - ...
    18.125*x^4 + 6.81293*x^5, ...
    0);
signal12 = par_chi * double(areaFunc(gammac)) * (1-kmrgd(2));
signal21 = par_chi * double(areaFunc(gammac)) * (1-kmrgd(1));
dydt=[(signal12^h)/(1+signal12^h)-kmrgd(1);
(signal21^h)/(1+signal21^h)-kmrgd(2);];

% --------------------------------------------------------------------------
function [tspan,y0,options] = init
handles = feval(AdaptiveDropletPair);
y0=[0,0];
options = odeset('Jacobian',[],'JacobianP',[],'Hessians',[],'HessiansP',[]);
tspan = [0 10];

% --------------------------------------------------------------------------
function jac = jacobian(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function jacp = jacobianp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hess = hessians(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hessp = hessiansp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens3  = der3(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens4  = der4(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens5  = der5(t, kmrgd, par_gammaA, par_chi, par_gamma0)

