% File demonstrating how to perform a continuation of a hopf bifurcation
% (i.e. following a codim-1 bifurcation point in codim-2)
% note that init and cont functions store overall structure of the sytem in
% a global variable called cds

% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName ' output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = AdaptiveDropletPair;
OPTIONS=[];

% Initial variable values
u1Init = 0.4;
u2Init = 0.75;

% Initial parameter values
gamma0 = 0.90;
gammaA = gamma0;
chi = 40;

% Integrate in time
[t,y] = ode45(MySystem{2},[0 100],[u1Init u2Init],OPTIONS,gammaA,chi,gamma0);

% Show system trajectory in phase plane
plot(y(:,1),y(:,2))
hold on
xlim([0 1])
ylim([0 1])
xlabel('u1')
ylabel('u2')

% Show vector field in phase plane
% X,Y are the coordinates in the u1,u2 phase plane
[X,Y] = meshgrid(0:0.1:1, 0:0.1:1);
% U,V are the vector components of the flow field
U = zeros(size(X));
V = zeros(size(Y));
for i=1:size(X,1)
    for j=1:size(X,2)
        uv = feval(MySystem{2},0,[X(i,j) Y(i,j)], gammaA, chi, gamma0);
        U(i,j) = uv(1);
        V(i,j) = uv(2);
    end
end
% Vector plot
% quiver(X,Y,U,V,'red')
% Vector plot with normalized arrow length (all same)
Un=U./sqrt(U.^2+V.^2);
Vn=V./sqrt(U.^2+V.^2);
quiver(X,Y,Un,Vn,'red')
hold off

%% Find Hopf point starting from last point of trajectory
% list of current parameter values (vertical vector!)
p=[gammaA; chi; gamma0];
% active parameter in continuation
ap=[2];
% initial state values (vertical vector!)
x1 = [y(end,1);y(end,2)];
[x0,v0]=init_EP_EP(@AdaptiveDropletPair,x1,p,ap);

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',1000);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% perform continuation
disp('From EP')
opt=contset(opt,'Backward',0);
[x,v,s,h,f]=cont(@equilibrium,x0,[],opt);

% plot bifurcation diagram
% fig = gcf;
% ax = fig.CurrentAxes;
% plot(x(3,:),x(1,:),'b')
% plot(x(3,:),f(1,:).*f(2,:))

disp('From H')
% pick first limit point as start for continuation
hopf_points = s(strcmp({s.label}, 'H '));
% adjust active parameter value
p(ap)=x(3,hopf_points(1).index);
% initial state values (vertical vector!)
x1 = x(1:2,hopf_points(1).index);
% choose two active parameters
ap = [1,2];
% initialize continuation of Equilibrium
[x0,v0]=init_H_H(@AdaptiveDropletPair,x1,p,ap);
% adapt continuation options
opt=contset(opt,'InitStepsize',0.001);
opt=contset(opt,'MaxNumPoints',100);
opt=contset(opt,'Backward',0);
% perform continuation
[x,v,s,h,f]=cont(@hopf,x0,v0,opt); %ignore last row of x output

% Export bifurcation points
% select only one of the two hopf branches that are identical in the
% (chi,gammaA) state space
% we identify the Hopf point with smallest gammaA as the saddle-node
% pitchfork
[~, minIndex] = min(x(3, :));
data_H = x(3:4, 1:minIndex);
data_SP = data_H(:,end);
csvwrite(fullfile(savePath,'SP.csv'),data_SP)
csvwrite(fullfile(savePath,'H.csv'),data_H)

%% Plot bifurcation diagram
figure
fig = gcf;
ax = fig.CurrentAxes;
plot(data_H(1,:),data_H(2,:),'r')
xlim([0,gamma0])
ylim([0,40])