% note that init and cont functions store overall structure of the sytem in
% a global variable called cds

% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName '_output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = odefile;
OPTIONS=[];

% Initial parameter values
gamma0 = 0.95;
gammaA = gamma0-0.1;
chi = 0;
hill = 4;

% Initial variable values
uInit = 0.4;

% Integrate in time
[t,y] = ode45(MySystem{2},[0 50],[uInit],OPTIONS,gamma0,gammaA,chi,hill);

% Initialize equilibrium continuation from last point of trajectory,
p=[gamma0; gammaA; chi; hill];
% active parameter in continuation
ap=[3];
% initial state values (vertical vector!)
x1 = y(end)';
disp('From Eq')
[x0,v0]=init_EP_EP(@odefile,x1,p,ap);

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',1000);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'MaxStepsize',0.01);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% perform continuation
[x,v,s,h,f]=cont(@equilibrium,x0,[],opt);

% show equilibrium curve
plot(x(2,:),x(1,:))
hold on
xlim([0,1])
xlabel('chi')
ylabel('u*')

disp('Continuation from LP point')
% select LP points
limit_points = s(strcmp({s.label}, 'LP'));
% adjust active parameter value
p(ap)=x(2,limit_points(1).index);
% initial state values (vertical vector!)
x1 = x(1,limit_points(1).index);
% choose two active parameters
ap = [2,3];
% initialize continuation of Equilibrium
[x0,v0]=init_LP_LP(@odefile,x1,p,ap);
% adapt continuation options
opt=contset(opt,'InitStepsize',0.001);
opt=contset(opt,'MaxNumPoints',10000);
% perform continuation
[x,v,s,h,f]=cont(@limitpoint,x0,v0,opt);

% pick cusp point as start for continuation
cusp_points = s(strcmp({s.label}, 'CP'));
% adjust active parameter value
p(ap)=x(2:3,cusp_points(1).index);
% initial state values
x1 = x(1,cusp_points(1).index);
% initialize continuation of Equilibrium
[x0,v0]=init_LP_LP(@odefile,x1,p,ap);
% perform continuation (forward)
disp('Continuation from Cusp forward')
opt=contset(opt,'MaxNumPoints',10000);
[x1,v1,s1,h1,f1]=cont(@limitpoint,x0,v0,opt);
% perform continuation (backward)
opt=contset(opt,'Backward',1);
opt=contset(opt,'MaxNumPoints',10000);
disp('Continuation from Cusp backward')
[x2,v2,s2,h2,f2]=cont(@limitpoint,x0,v0,opt);

% select data with gammaA <= 1
data_CP = x(:,cusp_points(1).index);
data_LP1 = x1(:,(x1(2, :) > 0) & (x1(2, :) <= 1.1));
data_LP2 = x2(:,(x2(2, :) > 0) & (x2(2, :) <= 1.1));

% Export results
csvwrite(fullfile(savePath,"LP1_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_LP1)
csvwrite(fullfile(savePath,"LP2_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_LP2)
csvwrite(fullfile(savePath,"Cusp_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_CP)

% Show results
% figure
% plot(data_LP1(2,:),data_LP1(3,:),'b')
% hold on
% plot(data_LP2(2,:),data_LP2(3,:),'b')
% scatter(data_CP(1),data_CP(2))
% xlim([0,1])
% ylim([0,1.5])