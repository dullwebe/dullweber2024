% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName '_output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = odefile;
OPTIONS=[];

% Initial parameter values
gamma0 = 0.95;
gammaA = 0.9;
chi = 0.05;
hill=2;

% Initial variable values
uInit = 0.4;

% Integrate in time
[t,y] = ode45(MySystem{2},[0 50],[uInit],OPTIONS,gamma0,gammaA,chi,hill);

% Show system trajectory
% plot(t,y)
% hold on
% xlim([0 t(end)])
% ylim([0 1])
% xlabel('t')
% ylabel('u')

%% Initialize equilibrium continuation from last point of trajectory,
% list of current parameter values (vertical vector!)
p=[gamma0; gammaA; chi; hill];
% active parameter in continuation
ap=[3];
% initial state values (vertical vector!)
x1 = y(end);
[x0,v0]=init_EP_EP(@odefile,x1,p,ap);

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',3000);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% perform continuation
[x,v,s,h,f]=cont(@equilibrium,x0,[],opt);

% select LP points
limit_points = s(strcmp({s.label}, 'LP'));

if length(limit_points) == 2

    % split curve into stable/unstable fixpoint branches
    data_EQ = x(2:-1:1, x(2, :) < gamma0);
    data_LP1 = x(2:-1:1,limit_points(1).index);
    data_LP2 = x(2:-1:1,limit_points(2).index);
    data_Stable1 = x(2:-1:1,1:limit_points(1).index);
    data_Unstable = x(2:-1:1,limit_points(1).index:limit_points(2).index);
    data_Stable2 = x(2:-1:1,limit_points(2).index:end);

    % Export branches and limit points
    csvwrite(fullfile(savePath,"Stable1_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_Stable1)
    csvwrite(fullfile(savePath,"Stable2_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_Stable2)
    csvwrite(fullfile(savePath,"Unstable_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_Unstable)
    csvwrite(fullfile(savePath,"LPPoint1_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_LP1)
    csvwrite(fullfile(savePath,"LPPoint2_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_LP2)
    csvwrite(fullfile(savePath,"EQ1_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"), data_EQ)
    
    %Show results
    figure
    hold on
    plot(data_Stable1(1,:),data_Stable1(2,:),'b')
    plot(data_Stable2(1,:),data_Stable2(2,:),'b')
    plot(data_Unstable(1,:),data_Unstable(2,:),'r')
    xlim([0,1])
    xlabel('gammaA')
    ylabel('u*')

else
    
    data_EQ = x(2:-1:1,:);
    csvwrite(fullfile(savePath,'EQ1.csv'), data_EQ)
   
    %Show results
    disp('No limit points found')
    figure
    hold on
    plot(data_EQ(1,:),data_EQ(2,:),'b')
    xlim([0,10])
    xlabel('chi')
    ylabel('u*')
    
end
