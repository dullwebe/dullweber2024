# MatCont

This directory contains Matlab scripts to run the numerical continuation analysis using MatCont presented in "Shape switching and tunable oscillations of adaptive droplets" Fig.2(a),3,S6,S7 and in the article "Feedback between microscopic activity and macroscopic dynamics drives excitability and oscillations in mechanochemical matter" Fig.3-7,11

In the following, we list the figure panels presented in the two articles and the corresponding directories containing the scripts to run the numerical continuation analysis. Each script creates an own output directory with its name + "_output" containing the continuation results in .csv format.

- "Shape switching and tunable oscillations of adaptive droplets"
	- Fig.2(a)
	In the directory "WetFoams" run the script continuationSN.m to compute the cusp points and associated saddle-node (SN) bifurcation lines for varying contact number n. For the inset, run continuationEq.m

	- Fig.3(a) and Fig.S5
	In the directory "AdaptiveDropletPair98", run ContinuationBP.m, ContinuationHopf.m and ContinuationSN.m to compute the pitch fork, Hopf and saddle-node (SHET) bifurcation lines, respectively. 

	- Fig.3(c) and Fig.S7
In the directory "AdaptiveDropletPair98", run the scripts containing "CloseToSP" in their name (SP1-SP4 for the bifurcation diagrams shown in Fig.S7 and Fig.3(c) top and bottom, SP5 for the 2. diagram in Fig.3(c)

	- Fig.S6
For panel (a) run the scripts in the directories "AdaptiveDropletPair95h3-h6". For panel (b) run the scripts in the directories "AdaptiveDropletPair92-98". 


- "Feedback between microscopic activity and macroscopic dynamics drives excitability and oscillations in mechanochemical matter"

	- Fig.3
For the inset of panel (a), in the directory "SingleDroplet", run continuationEq.m. For all other saddle-node bifurcation lines and cusp bifurcation points, run continuationSN.m after adjusting parameters (e.g. the Hill coefficient) at the beginning of the script.

	- Fig.4
In the directory "SymmetricDoublet", run continuationBP.m after adjusting the parameters (e.g. the Hill coefficient).

	- Fig.5 and Fig.10
This is identical to Fig.3(a) and Fig.S6 (panels g,h) of the companion paper (see above)

	- Fig.6
For the case of unequal droplet volumes, run ContinuationBP.m, ContinuationSN.m and ContinuationHopf.m from the directories "AdaptiveDropletPair_UnequalVolume025-05". For the case of unequal outer surface tensions, run ContinuationBP.m, ContinuationSN.m and ContinuationHopf.m from the directories "AdaptiveDropletPair_UnequalOuterTension025-05".
Note that the MATLAB Symbolic Math Toolbox is necessary to run these scripts!

	- Fig.7
For figure panel (a), run ContinuationHom1.m, ContinuationHom2.m, ContinuationHopf1.m, ContinuationHopf2.m, ContinuationSN1.m, ContinuationSN2.m from the directory "AdaptiveDropletPair_UnequalSusceptibility". For figure panel (c), run Continuation_gA0245.m and Continuation_gA0637.

	- Fig.11
This is identical to Fig.S7 of the companion paper (see above).
