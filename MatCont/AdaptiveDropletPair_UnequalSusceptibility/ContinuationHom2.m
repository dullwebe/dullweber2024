% Strategy - Coming from left via Hopf close to BT

% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName ' output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = AsymmetricAdaptiveDropletPair;
OPTIONS=[];

% Initial variable values
u1Init = 0.4;
u2Init = 0.75;

% Initial parameter values
gamma0 = 0.98;
chi0 =  40.604;
BT = [0.39461, 21.883];
gammaA = 0.399*gamma0;
%chi = 0.5395*chi0;
chi = 0.53915*chi0;

% Integrate in time
[t,y] = ode45(MySystem{2},[0 50],[u1Init u2Init],OPTIONS,gammaA,chi,gamma0);

% Show system trajectory in phase plane
plot(y(:,1),y(:,2))
hold on
xlim([0 1])
ylim([0 1])
xlabel('u1')
ylabel('u2')

% Show vector field in phase plane
% X,Y are the coordinates in the u1,u2 phase plane
[X,Y] = meshgrid(0:0.1:1, 0:0.1:1);
% U,V are the vector components of the flow field
U = zeros(size(X));
V = zeros(size(Y));
for i=1:size(X,1)
    for j=1:size(X,2)
        uv = feval(MySystem{2},0,[X(i,j) Y(i,j)], gammaA, chi, gamma0);
        U(i,j) = uv(1);
        V(i,j) = uv(2);
    end
end
% Vector plot
% quiver(X,Y,U,V,'red')
% Vector plot with normalized arrow length (all same)
Un=U./sqrt(U.^2+V.^2);
Vn=V./sqrt(U.^2+V.^2);
quiver(X,Y,Un,Vn,'red')
hold off

%% Find Limit point starting from last point of trajectory
% list of current parameter values (vertical vector!)
p=[gammaA; chi; gamma0];
% active parameter in continuation
ap=[1];
% initial state values (vertical vector!)
xInit = [y(end,1);y(end,2)];
[x0,v0]=init_EP_EP(@AsymmetricAdaptiveDropletPair,xInit,p,ap);

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',1000);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% perform continuation
disp('From EP')
[x,v,s,h,f]=cont(@equilibrium,x0,[],opt);
hopf_points = s(strcmp({s.label}, 'H '));

disp('LC from Hopf')
% adjust active parameter value
p(ap)=x(3,hopf_points(1).index);
% initial state values
xInit = x(1:2,hopf_points(1).index);
% initialize continuation of Equilibrium
opt=contset(opt,'InitStepsize',0.05);
[x0,v0]=init_H_LC(@AsymmetricAdaptiveDropletPair,xInit,p,ap,1e-6,20,4);
% perform continuation
opt=contset(opt,'MaxNumPoints',500);
opt = contset(opt, 'Multipliers',1);
opt = contset(opt, 'Adapt',1);
[x1,v1,s1,h1,f1]=cont(@limitcycle,x0,v0,opt);

disp('From LC - Continue Homoclinic ')
% adjust active parameter value
p(ap)=x1(end,end);
ap = [1,2];
T=x1(end-1,end)/2;
% initialize continuation of homoclinic
opt=contset(opt,'InitStepsize',0.07);
[x0,v0]=init_LC_Hom(@AsymmetricAdaptiveDropletPair,x1(:,end),s1(:,end),...
    p,ap,40,4,[0,1,1],T,0.01,0.01);
% perform continuation
opt=contset(opt,'MinStepsize',0.001);
opt=contset(opt,'MaxStepsize',0.001);
opt=contset(opt,'MaxNumPoints',500);
opt=contset(opt,'Singularities',0);
opt = contset(opt, 'Adapt',0);
opt=contset(opt,'Backward',1);
[x2,v2,s2,h2,f2]=cont(@homoclinic,x0,v0,opt);
opt=contset(opt,'Backward',0);

data_Hom2 = x2(end-5:end-4,:);
csvwrite(fullfile(savePath,'Hom2.csv'),data_Hom2)