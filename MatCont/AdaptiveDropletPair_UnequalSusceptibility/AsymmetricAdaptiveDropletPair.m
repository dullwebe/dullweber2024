function out = AsymmetricAdaptiveDropletPair
out{1} = @init;
out{2} = @fun_eval;
% out{3} = @jacobian;
% out{4} = @jacobianp;
% out{5} = @hessians;
% out{6} = @hessiansp;
out{3} = [];
out{4} = [];

out{5} = [];
out{6} = [];
out{7} = [];
out{8} = [];
out{9} = [];

% --------------------------------------------------------------------------
function dydt = fun_eval(t, kmrgd, par_gammaA, par_chi, par_gamma0)
h=4;
% consider gamma0 as gamma0/2gammaf
% consider gammaA as gammaA/2gammaf
% consider gammac as gammac/2gammaf
delta_chi = 0.05*par_chi;
chi1 = par_chi+delta_chi;
chi2 = par_chi-delta_chi;
gammac = par_gamma0 - par_gammaA*kmrgd(1)*kmrgd(2); 
area = (1-gammac^2)*(2/((2-gammac)*(1+gammac)^2))^(2/3);
signal12 = chi1 * area*(1-kmrgd(2));
signal21 = chi2 * area*(1-kmrgd(1));
dydt=[(signal12^h)/(1+signal12^h)-kmrgd(1);
(signal21^h)/(1+signal21^h)-kmrgd(2);];

% --------------------------------------------------------------------------
function [tspan,y0,options] = init
handles = feval(AdaptiveDropletPair);
y0=[0,0];
options = odeset('Jacobian',[],'JacobianP',[],'Hessians',[],'HessiansP',[]);
tspan = [0 10];

% --------------------------------------------------------------------------
function jac = jacobian(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function jacp = jacobianp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hess = hessians(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hessp = hessiansp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens3  = der3(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens4  = der4(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens5  = der5(t, kmrgd, par_gammaA, par_chi, par_gamma0)