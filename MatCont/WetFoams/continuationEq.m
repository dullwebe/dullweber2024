% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName '_output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = odefile;
OPTIONS=[];

% Initial variable values
uInit = 0.4;

% Initial parameter values
chi = 0.0065;
gamma0 = 0.87;
gammaA = 0.14;
n=6;
hill = 4;

% Integrate in time
[t,y] = ode45(MySystem{2},[0 50],uInit,OPTIONS,gammaA,chi,gamma0,n,hill);

% Show system trajectory
% plot(t,y)
% hold on
% xlim([0 t(end)])
% ylim([0 1])
% xlabel('t')
% ylabel('u')

%% Initialize equilibrium continuation from last point of trajectory,
% list of current parameter values (vertical vector!)
p=[gammaA; chi; gamma0; n; hill];
% active parameter in continuation
ap=[2];
% initial state values (vertical vector!)
x1 = y(end);
[x0,v0]=init_EP_EP(@odefile,x1,p,ap);

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',2000);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% perform continuation
[x,v,s,h,f]=cont(@equilibrium,x0,[],opt);

% select LP points
limit_points = s(strcmp({s.label}, 'LP'));

if length(limit_points) == 2

    % split curve into stable/unstable fixpoint branches
    data_EQ = x;
    data_LP1 = x(:,limit_points(1).index);
    data_LP2 = x(:,limit_points(2).index);
    data_Stable1 = x(:,1:limit_points(1).index);
    data_Unstable = x(:,limit_points(1).index:limit_points(2).index);
    data_Stable2 = x(:,limit_points(2).index:end);

    % Export branches and limit points
    csvwrite(fullfile(savePath,strjoin({'Stable1_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_Stable1)
    csvwrite(fullfile(savePath,strjoin({'Stable2_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_Stable2)
    csvwrite(fullfile(savePath,strjoin({'Unstable_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_Unstable)
    csvwrite(fullfile(savePath,strjoin({'LPPoint1_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_LP1)
    csvwrite(fullfile(savePath,strjoin({'LPPoint2_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_LP2)
    csvwrite(fullfile(savePath,strjoin({'EQ1_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')), data_EQ)
    
    %Show results
    figure
    hold on
    plot(data_Stable1(2,:),data_Stable1(1,:),'b')
    plot(data_Stable2(2,:),data_Stable2(1,:),'b')
    plot(data_Unstable(2,:),data_Unstable(1,:),'r')
    xlim([0,2])
    xlabel('chi')
    ylabel('u*')

elseif length(limit_points) == 4

    % split curve into stable/unstable fixpoint branches
    data_EQ = x(:, :);
    data_LP1 = x(:,limit_points(3).index);
    data_LP2 = x(:,limit_points(4).index);
    data_Stable1 = x(:,1:limit_points(3).index);
    data_Unstable = x(:,limit_points(3).index:limit_points(4).index);
    data_Stable2 = x(:,limit_points(4).index:end);

    % Export branches and limit points
    csvwrite(fullfile(savePath,strjoin({'Stable1_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_Stable1)
    csvwrite(fullfile(savePath,strjoin({'Stable2_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_Stable2)
    csvwrite(fullfile(savePath,strjoin({'Unstable_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_Unstable)
    csvwrite(fullfile(savePath,strjoin({'LPPoint1_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_LP1)
    csvwrite(fullfile(savePath,strjoin({'LPPoint2_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')),data_LP2)
    csvwrite(fullfile(savePath,strjoin({'EQ1_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')), data_EQ)
    
    %Show results
    figure
    hold on
    plot(data_Stable1(2,:),data_Stable1(1,:),'b')
    plot(data_Stable2(2,:),data_Stable2(1,:),'b')
    plot(data_Unstable(2,:),data_Unstable(1,:),'r')
    xlim([0,2])
    xlabel('chi')
    ylabel('u*')

else
    
    data_EQ = x(:,:);
    csvwrite(fullfile(savePath,strjoin({'EQ1_g0',num2str(gamma0),'_gA',num2str(gammaA),'_n',num2str(n),'_h',num2str(hill),'.csv'},'')), data_EQ)
   
    %Show results
    figure
    hold on
    plot(data_EQ(2,:),data_EQ(1,:),'b')
    xlim([0,2])
    xlabel('chi')
    ylabel('u*')

end

