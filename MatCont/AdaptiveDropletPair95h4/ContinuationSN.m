% File demonstrating how to perform a continuation of a saddle-node + cusp
% (i.e. following a codim-1 bifurcation point in codim-2)
% note that init and cont functions store overall structure of the sytem in
% a global variable called cds

% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName '_output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = AdaptiveDropletPair95;
OPTIONS=[];

% Initial variable values
u1Init = 0.4;
u2Init = 0.75;

% Initial parameter values
gamma0 = 0.95;
gammaA = 0.94;
chi = 1;

% Integrate in time
[t,y] = ode45(MySystem{2},[0 50],[u1Init u2Init],OPTIONS,gammaA,chi,gamma0);

% Show system trajectory in phase plane
plot(y(:,1),y(:,2))
hold on
xlim([0 1])
ylim([0 1])
xlabel('u1')
ylabel('u2')

% Show vector field in phase plane
% X,Y are the coordinates in the u1,u2 phase plane
[X,Y] = meshgrid(0:0.1:1, 0:0.1:1);
% U,V are the vector components of the flow field
U = zeros(size(X));
V = zeros(size(Y));
for i=1:size(X,1)
    for j=1:size(X,2)
        uv = feval(MySystem{2},0,[X(i,j) Y(i,j)], gammaA, chi, gamma0);
        U(i,j) = uv(1);
        V(i,j) = uv(2);
    end
end
% Vector plot
% quiver(X,Y,U,V,'red')
% Vector plot with normalized arrow length (all same)
Un=U./sqrt(U.^2+V.^2);
Vn=V./sqrt(U.^2+V.^2);
quiver(X,Y,Un,Vn,'red')
hold off

%% Find Limit point starting from last point of trajectory
% list of current parameter values (vertical vector!)
p=[gammaA; chi; gamma0];
% active parameter in continuation
ap=[2];
% initial state values (vertical vector!)
x1 = [y(end,1);y(end,2)];
[x0,v0]=init_EP_EP(@AdaptiveDropletPair95,x1,p,ap);

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',500);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% perform continuation
disp('From EP')
[x,v,s,h,f]=cont(@equilibrium,x0,[],opt);

% plot bifurcation diagram
% fig = gcf;
% ax = fig.CurrentAxes;
% plot(x(3,:),x(1,:),'b')
% plot(x(3,:),f(1,:).*f(2,:))

disp('From LP')
% pick first limit point as start for continuation
limit_points = s(strcmp({s.label}, 'LP'));
% adjust active parameter value
p(ap)=x(3,limit_points(1).index);
% initial state values (vertical vector!)
x1 = x(1:2,limit_points(1).index);
% choose two active parameters
ap = [1,2];
% initialize continuation of Equilibrium
[x0,v0]=init_LP_LP(@AdaptiveDropletPair95,x1,p,ap);
% adapt continuation options
opt=contset(opt,'InitStepsize',0.001);
opt=contset(opt,'MaxNumPoints',5000);
% perform continuation
[x,v,s,h,f]=cont(@limitpoint,x0,v0,opt);

% pick cusp point as start for continuation
cusp_points = s(strcmp({s.label}, 'CP'));
% adjust active parameter value
p(ap)=x(3:4,cusp_points(1).index);
% initial state values
x1 = x(1:2,cusp_points(1).index);
% initialize continuation of Equilibrium
[x0,v0]=init_LP_LP(@AdaptiveDropletPair95,x1,p,ap);
% perform continuation (forward)
disp('From Cusp forward')
opt=contset(opt,'MaxNumPoints',1000);
[x1,v1,s1,h1,f1]=cont(@limitpoint,x0,v0,opt);
% perform continuation (backward)
opt=contset(opt,'Backward',1);
opt=contset(opt,'MaxNumPoints',4000);
disp('From Cusp backward')
[x2,v2,s2,h2,f2]=cont(@limitpoint,x0,v0,opt);

% Export bifurcation points
% select data with gammaA <= gamma0
data_CP = x(3:4,cusp_points(1).index);
%data_LP1 = x1(3:4,(x1(3, :) > 0) & (x1(3, :) <= gamma0));
%data_LP2 = x2(3:4,(x2(3, :) > 0) & (x2(3, :) <= gamma0));
data_LP1 = x1(3:4,(x1(3, :) > 0));
data_LP2 = x2(3:4,(x2(3, :) > 0));
csvwrite(fullfile(savePath,'LP1.csv'),data_LP1)
csvwrite(fullfile(savePath,'LP2.csv'),data_LP2)
csvwrite(fullfile(savePath,'Cusp.csv'),data_CP)

%% plot bifurcation diagram
figure
fig = gcf;
ax = fig.CurrentAxes;
plot(data_LP1(1,:),data_LP1(2,:),'k')
hold on
plot(data_LP2(1,:),data_LP2(2,:),'k')
xlim([0,gamma0])
ylim([0,40])