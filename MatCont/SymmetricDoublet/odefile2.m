function out = odefile2
out{1} = @init;
out{2} = @fun_eval;
% out{3} = @jacobian;
% out{4} = @jacobianp;
% out{5} = @hessians;
% out{6} = @hessiansp;
out{3} = [];
out{4} = [];
out{5} = [];
out{6} = [];
out{7} = [];
out{8} = [];
out{9} = [];

% --------------------------------------------------------------------------
function dydt = fun_eval(t, kmrgd, par_gamma0, par_gammaA, par_chi, par_hill)
u0 = 0.05;
% consider gamma0 as gamma0/2gammaf
% consider gammaA as gammaA/2gammaf
% consider gammac as gammac/2gammaf
gammac = par_gamma0 - par_gammaA*kmrgd(1)*kmrgd(2); 
%disp([gammac,kmrgd(1),kmrgd(2)]);
area = (1-gammac^2)*(2/((2-gammac)*(1+gammac)^2))^(2/3);
%signal12 = par_chi*area*(u0+kmrgd(2));
%signal21 = par_chi*area*(u0+kmrgd(1));
signal12 = par_chi*area*(1-kmrgd(2));
signal21 = par_chi*area*(1-kmrgd(1));
%disp([signal12,signal21])
%disp(par_hill)
dydt=[(signal12^par_hill)/(1+signal12^par_hill)-kmrgd(1);
(signal21^par_hill)/(1+signal21^par_hill)-kmrgd(2);];

% --------------------------------------------------------------------------
function [tspan,y0,options] = init
handles = feval(AdaptiveDropletPair);
y0=[0,0];
options = odeset('Jacobian',[],'JacobianP',[],'Hessians',[],'HessiansP',[]);
tspan = [0 10];

% --------------------------------------------------------------------------
function jac = jacobian(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function jacp = jacobianp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hess = hessians(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hessp = hessiansp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens3  = der3(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens4  = der4(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens5  = der5(t, kmrgd, par_gammaA, par_chi, par_gamma0)
