% File demonstrating how to perform a continuation in 2D state space for
% branch points - has to be done iteratively, as BP_BP does not work here

% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName '_output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = odefile2;
OPTIONS=[];

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',500);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'MaxStepsize',0.1);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% list of gammaA values
gamma0 = 0.95;
gammaA_values = 0:0.01:gamma0;
hill=6;
%store branch points
branch_points = [];
eigenvalues = [];

for gammaA=gammaA_values
    try
        % Initial variable values
        u1Init = 0.4;
        u2Init = 0.75;

        % Initial parameter value
        chi = 1;

        % Integrate in time
        [t,y] = ode45(MySystem{2},[0 50],[u1Init u2Init],OPTIONS,gamma0,gammaA,chi,hill);


        % list of current parameter values (vertical vector!)
        p=[gamma0; gammaA; chi; hill];
        % active parameter in continuation
        ap=[3];
        % initial state values (vertical vector!)
        x1 = [y(end,1);y(end,2)];
        % initialize continuation of Equilibrium
        [x0,v0]=init_EP_EP(@odefile2,x1,p,ap);
        % perform continuation
        opt=contset(opt,'Backward',0);
        [x,v,s,h,f]=cont(@equilibrium,x0,[],opt);

        %extract branch_point
%         if sum(strcmp({s.label}, 'BP')) > 1
%             error("More than one BP detected")
%         end
        bp = s(strcmp({s.label}, 'BP'));
        bp = bp(1);
        
        % Save branch point
        branch_points = [branch_points, x(:,bp.index)];
        eigenvalues = [eigenvalues, f(:,bp.index-3)];
    catch
        branch_points = [branch_points, [-1;-1;x(3,1)]];
        eigenvalues = [eigenvalues, [0;0]];
    end
   
   
end

% Sort into stable (supercrit) and unstable (subcrit) BPs
stability = max(eigenvalues);
supercritBP = branch_points(:, stability < 0);
subcritBP = branch_points(:, stability > 0);

%% Plot results
%plot(gammaA_values,branch_points(3,:))
% supercrit BP
plot(gammaA_values(stability < 0),supercritBP(3,:),'b')
hold on
% subcrit BP
plot(gammaA_values(stability > 0),subcritBP(3,:), 'r')
% plot options
xlabel('gammaA')
ylabel('chi')
xlim([0,gamma0])
ylim([0,40])

%% Export BP curves as csv, 1st row: gammaA, 2nd row: chi
data_supercritBP = [gammaA_values(stability < 0); supercritBP];
data_subcritBP = [gammaA_values(stability > 0); subcritBP];
csvwrite(fullfile(savePath,"supercritBP_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_supercritBP)
csvwrite(fullfile(savePath,"subcritBP_g0"+num2str(gamma0)+"_h"+num2str(hill)+".csv"),data_subcritBP)