function out = AsymmetricAdaptiveDropletPair
out{1} = @init;
out{2} = @fun_eval;
% out{3} = @jacobian;
% out{4} = @jacobianp;
% out{5} = @hessians;
% out{6} = @hessiansp;
out{3} = [];
out{4} = [];

out{5} = [];
out{6} = [];
out{7} = [];
out{8} = [];
out{9} = [];

% --------------------------------------------------------------------------
function dydt = fun_eval(t, kmrgd, par_gammaA, par_chi, par_gamma0)
h=4;
% consider gamma0 as gamma0/2gammaf
% consider gammaA as gammaA/2gammaf
% consider gammac as gammac/2gammaf
gammac = par_gamma0 - par_gammaA*kmrgd(1)*kmrgd(2); 
syms areaFunc(x);
areaFunc(x) = piecewise( ...
    x>0 & x <= 0.25, 2^(4/3), ...
    x>0.25 & x <= 0.425, -0.785751+1.09021/(x-0.246746).^0.193864, ...
    x>0.425 & x<1, 1.92106 - 4.1884*x + 3.47453*x.^2 ...
    + 0.623755*x.^3 - 3.21614*x.^4 + 1.38592*x.^5, ...
    0);
signal12 = par_chi * double(areaFunc(gammac)) * (1-kmrgd(2));
signal21 = par_chi * double(areaFunc(gammac)) * (1-kmrgd(1));
dydt=[(signal12^h)/(1+signal12^h)-kmrgd(1);
(signal21^h)/(1+signal21^h)-kmrgd(2);];

% --------------------------------------------------------------------------
function [tspan,y0,options] = init
handles = feval(AdaptiveDropletPair);
y0=[0,0];
options = odeset('Jacobian',[],'JacobianP',[],'Hessians',[],'HessiansP',[]);
tspan = [0 10];

% --------------------------------------------------------------------------
function jac = jacobian(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function jacp = jacobianp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hess = hessians(t, kmrgd, par_gammaA, par_chi, par_gamma0)
% --------------------------------------------------------------------------
function hessp = hessiansp(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens3  = der3(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens4  = der4(t, kmrgd, par_gammaA, par_chi, par_gamma0)
%---------------------------------------------------------------------------
function tens5  = der5(t, kmrgd, par_gammaA, par_chi, par_gamma0)