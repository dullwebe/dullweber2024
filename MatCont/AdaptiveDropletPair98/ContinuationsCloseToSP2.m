% File demonstrating how to perform a continuation
% (i.e. following a codim-1 bifurcation point)
% note that init and cont functions store overall structure of the sytem in
% a global variable called cds

% Get path and name of script
[scriptPath, scriptName, ~] = fileparts(mfilename('fullpath'));
% set scriptPath current directory
cd(scriptPath);
% get parent directory and add MatCont files to search path
parentDirectory = fileparts(scriptPath);
matContPath = fullfile(parentDirectory, 'MatCont7p4');
addpath(genpath(matContPath));

% Create folder to save outputs
newFolderName = [scriptName ' output'];
savePath = fullfile(scriptPath, newFolderName);
if ~exist(savePath, 'dir')
    mkdir(savePath);
end

% Load system
MySystem = AdaptiveDropletPair;
OPTIONS=[];

% Initial variable values
u1Init = 0.4;
u2Init = 0.75;

% Initial parameter values
chi0 = 40.604;
gamma0 = 0.98;
gammaA = 0.13*gamma0;
chi = 0.68*chi0;

% Range I am studying
gammaAMin = 0.13*gamma0;
gammaAMax = 0.25*gamma0;

% Integrate in time
[t,y] = ode45(MySystem{2},[0 100],[u1Init u2Init],OPTIONS,gammaA,chi,gamma0);

% Show system trajectory in phase plane
plot(y(:,1),y(:,2))
hold on
xlim([0 1])
ylim([0 1])
xlabel('u1')
ylabel('u2')

% Show vector field in phase plane
% X,Y are the coordinates in the u1,u2 phase plane
[X,Y] = meshgrid(0:0.1:1, 0:0.1:1);
% U,V are the vector components of the flow field
U = zeros(size(X));
V = zeros(size(Y));
for i=1:size(X,1)
    for j=1:size(X,2)
        uv = feval(MySystem{2},0,[X(i,j) Y(i,j)], gammaA, chi, gamma0);
        U(i,j) = uv(1);
        V(i,j) = uv(2);
    end
end
% Vector plot
% quiver(X,Y,U,V,'red')
% Vector plot with normalized arrow length (all same)
Un=U./sqrt(U.^2+V.^2);
Vn=V./sqrt(U.^2+V.^2);
quiver(X,Y,Un,Vn,'red')
hold off

%% Initialize equilibrium continuation from last point of trajectory,
% list of current parameter values (vertical vector!)
p=[gammaA; chi; gamma0];
% active parameter in continuation
ap=[1];
% initial state values (vertical vector!)
[x0,v0]=init_EP_EP(@AdaptiveDropletPair,[y(end,1);y(end,2)],p,ap);

% options for continuation
opt=contset;
opt=contset(opt,'MaxNumPoints',1000);
opt=contset(opt,'InitStepsize',0.0001);
opt=contset(opt,'MaxStepsize',0.001);
opt=contset(opt,'Singularities',1);
opt=contset(opt,'Eigenvalues',1);

% perform continuation from initial EQ - FIRST STABLE BRANCH
[x1,v1,s1,h1,f1]=cont(@equilibrium,x0,[],opt);
limit_points = s1(strcmp({s1.label}, 'LP'));
branch_points = s1(strcmp({s1.label}, 'BP'));

% continue from BP1
x2Init = x1(1:2,branch_points(1).index);
p(ap) = x1(3,branch_points(1).index);
% initialize continuation, last argument is initial step size
[x0,v0]=init_BP_EP(@AdaptiveDropletPair,x2Init,p,branch_points(1),0.01);
% forward continuation
[x2a,v2a,s2a,h2a,f2a]=cont(@equilibrium,x0,v0,opt);
% backward continuation
opt=contset(opt,'Backward',1);
[x2b,v2b,s2b,h2b,f2b]=cont(@equilibrium,x0,v0,opt);
opt=contset(opt,'Backward',0);
hopf_points2a = s2a(strcmp({s2a.label}, 'H '));
hopf_points2b = s2b(strcmp({s2b.label}, 'H '));

% Select branches
data_Stable1 = x1(3:-2:1, 1:branch_points(1).index);
data_Saddle1 = x1(3:-2:1, branch_points(1).index:limit_points(1).index);
data_Unstable1 = x1(3:-2:1, limit_points(1).index:limit_points(2).index);
data_Saddle2 = x1(3:-2:1, limit_points(2).index:end);
data_Stable2 = x2a(3:-2:1, 1:hopf_points2a(1).index);
data_Unstable2 = x2a(3:-2:1, hopf_points2a(1).index:end);
data_Stable3 = x2b(3:-2:1, 1:hopf_points2b(1).index);
data_Unstable3 = x2b(3:-2:1, hopf_points2b(1).index:end);
data_LP1 = x1(3:-2:1, limit_points(1).index);
data_LP2 = x1(3:-2:1, limit_points(2).index);
data_BP = x1(3:-2:1, branch_points(1).index);
data_H1 = x2a(3:-2:1, hopf_points2a(1).index);
data_H2 = x2b(3:-2:1, hopf_points2b(1).index);

% plot bifurcation diagram
figure
hold on
plot(data_Stable1(1,:),data_Stable1(2,:),'b')
plot(data_Stable2(1,:),data_Stable2(2,:),'b')
plot(data_Stable3(1,:),data_Stable3(2,:),'b')
plot(data_Unstable1(1,:), data_Unstable1(2,:),'r')
plot(data_Unstable2(1,:), data_Unstable2(2,:),'r')
plot(data_Unstable3(1,:), data_Unstable3(2,:),'r')
plot(data_Saddle1(1,:), data_Saddle1(2,:),'r--')
plot(data_Saddle2(1,:), data_Saddle2(2,:),'r--')
xlim([0.13,0.25])

% Export results
csvwrite(fullfile(savePath,'Stable1.csv'),data_Stable1)
csvwrite(fullfile(savePath,'Stable2.csv'),data_Stable2)
csvwrite(fullfile(savePath,'Stable3.csv'),data_Stable3)
csvwrite(fullfile(savePath,'Saddle1.csv'),data_Saddle1)
csvwrite(fullfile(savePath,'Saddle2.csv'),data_Saddle2)
csvwrite(fullfile(savePath,'Unstable1.csv'),data_Unstable1)
csvwrite(fullfile(savePath,'Unstable2.csv'),data_Unstable2)
csvwrite(fullfile(savePath,'Unstable3.csv'),data_Unstable3)
csvwrite(fullfile(savePath,'LP1.csv'),data_LP1)
csvwrite(fullfile(savePath,'LP2.csv'),data_LP2)
csvwrite(fullfile(savePath,'BP.csv'),data_BP)
csvwrite(fullfile(savePath,'H1.csv'),data_H1)
csvwrite(fullfile(savePath,'H2.csv'),data_H2)