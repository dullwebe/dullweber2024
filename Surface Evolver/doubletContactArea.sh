#! /bin/bash

# delete output file 
if test -f "doublet_output.csv"; 
then 
  rm doublet_output.csv
fi

# measure interface area for different surface tensions at contacts of body bb
bb=1;
n=0;
for i in $(seq 2 -0.1 0.1)
do
  # Write surface tension at the contact surfaces into output file
  echo -n "$i," >> doublet_output.csv  
  # Write commands for surface evolver to set tension & minimise energy
  echo "quiet;
s
q
set_contact_tension($i);
gogo2;
print_contact_area($bb) >> \"doublet_output.csv\";" > myCommands.txt

  # Save x3d images of final shapes, can be visualized e.g. in Mathematica
  #echo "read \"x3d.cmd\";
#x3d >>> \"/Users/dullwebe/Evolver270-OSX/shapes/doublet_${i}.x3d\";" >> myCommands.txt

  # run evolver with commands saved in myCommands.txt
  evolver -f myCommands.txt -r "exit 0;" cubic_lattice_1_2_1.fe

#rm myCommands.txt
done


