# Surface Evolver

This directory contains the codes to reproduce the numerical surface minimization of different droplet configurations, presented in "Shape switching and tunable oscillations of adaptive droplets" Fig.1.

The Mathematica notebook "CreateInitializationFile.nb" allows to create initialization files (.fe) for Surface Evolver (Software can be found here for download: [Surface Evolver](http://kenbrakke.com/evolver/html/install.htm)), containing the vertices, edges and faces of arbitrary 3D lattice configurations of droplets as well as some useful functions to measure and manipulate the system.

The four bash scripts call their corresponding .fe files to minimize the surface energy and measure the contact area for varying surface tensions of a droplet pair (n=1), a line of 7 droplets (n=2), a 5x5 square lattice (n=4) and a 5x5x5 cubic lattice (n=6).