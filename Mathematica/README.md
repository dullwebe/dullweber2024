# Mathematica

This directory contains two Mathematica notebooks, that allow to reproduce most of the figures presented in the two articles.

The notebook "Numerical_computations_letter.nb" contains the code for the letter "Shape switching and tunable oscillations of adaptive droplets".

The notebook "Numerical_computations_article.nb" contains the code for the article "Feedback between microscopic activity and macroscopic dynamics drives excitability and oscillations in mechanochemical matter". Note that Figs.5,9-11 are mostly identical to Fig.3 and Figs.S4-S7 in the companion article and thus not contained in this notebook.